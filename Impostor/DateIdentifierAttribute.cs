﻿using System;

namespace Impostor.Attributes
{
    /// <summary>
    /// Identifies a date property
    /// </summary>
    public class DateIdentifierAttribute : IdentifierAttribute
    {
        /// <summary>
        /// Constructs a DateTime preference object
        /// </summary>
        /// <param name="name">The resource identifier</param>
        /// <param name="defaultValue">A yyyy-MM-dd formatted string, or a keyword like 'min', 'max' or 'now'</param>
        public DateIdentifierAttribute(string name, string defaultValue) : base(name, CalculateDateParam(defaultValue))
        {
        }

        /// <summary>
        /// Parses the date using the yyyy-MM-dd format and handling the now, min and max keywords
        /// </summary>
        /// <param name="defaultValue">the string passed as default value</param>
        /// <returns>The related datetime object</returns>
        private static DateTime CalculateDateParam(string defaultValue)
        {
            switch (defaultValue)
            {
                case "now": return DateTime.Now;
                case "min": return DateTime.MinValue;
                case "max": return DateTime.MaxValue;
                default: return DateTime.ParseExact(defaultValue, "yyyy-MM-dd", null);
            }
        }
    }
}