---
sidebar_position: 4
---

# Impostor Manager Lifecycle 

In order to use Impostor inside your whole application, you should be a using a **single instance** of the **Impostor Manager**. The simplest way is declaring it as a static property that is instanciated only when the application starts.

```cs
internal static ImpostorManager Manager { get; set; } = new ImpostorManager(new PreferencesImplementation());
```

## Using a DependencyInjection pattern

A better approach involves the usage of a **Dependency Injection** framework. In this case, both the `PreferencesImplementation` and the `ImpostorManager` can be registered as **singleton services** and then called anywhere from your code.

Here's an example:

```cs
using Microsoft.Extensions.DependencyInjection;

internal static IServiceProvider ServiceProvider { get; set; }

// Should be called when the application starts
private void SetupServices()
{
    var services = new ServiceCollection();
    services.AddSingleton<IPreferences, PreferencesImplementation>();
    services.AddSingleton<ImpostorManager>();
    ServiceProvider = services.BuildServiceProvider();
}
```

You can then require the Impostor Manager wherever you want:

```cs
var manager = ServiceProvider.GetService<ImpostorManager>();
```

