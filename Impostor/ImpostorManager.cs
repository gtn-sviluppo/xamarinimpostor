﻿using Impostor.Attributes;
using Impostor.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Xamarin.Essentials.Interfaces;

namespace Impostor
{
    /// <summary>
    /// Manages preferences through objects that are hooked at runtime
    /// </summary>
    public class ImpostorManager
    {
        private static IPreferences _preferences;
        private Dictionary<int, object> _cache;


        /// <summary>
        /// Instantiates a new Impostor Manager given a preferences source
        /// </summary>
        /// <param name="preferences">A preference storage object</param>
        public ImpostorManager(IPreferences preferences)
        {
            _preferences = preferences;
            _cache = new Dictionary<int, object>();

            ForceXamarinSkipLink();
        }

        private void ForceXamarinSkipLink()
        {
#pragma warning disable 0219, 0649
            bool falseFlag = false;

            if (falseFlag)
            {
                IPreferences prefs = new Xamarin.Essentials.Implementation.PreferencesImplementation();

                prefs.Get("str", "");
                prefs.Set("str", "");
                prefs.Remove("str");

                prefs.Get("int", 0);
                prefs.Set("int", 1);
                prefs.Remove("int");

                prefs.Get("double", 0d);
                prefs.Set("double", 1d);
                prefs.Remove("double");

                prefs.Get("long", 0L);
                prefs.Set("long", 1L);
                prefs.Remove("long");

                prefs.Get("float", 0f);
                prefs.Set("float", 1f);
                prefs.Remove("float");

                prefs.Get("date", DateTime.MinValue);
                prefs.Set("date", DateTime.Now);
                prefs.Remove("date");

                prefs.Get("bool", false);
                prefs.Set("bool", true);
                prefs.Remove("bool");
            }
#pragma warning restore 0219, 0649
        }

        /// <summary>
        /// Hooks a class to the manager in order to access data through its properties
        /// </summary>
        /// <typeparam name="T">The type of the class</typeparam>
        /// <returns></returns>
        public T Hook<T>() where T : class
        {
            var hash = typeof(T).GetHashCode();

            object instance = CacheInstance(typeof(T), hash);

            T castedInstance = (T)instance;

            return castedInstance;
        }


        /// <summary>
        /// Reads properties at runtime
        /// </summary>
        /// <param name="prop">property that will be read</param>
        /// <param name="startIdentifier">A prefix for the name of the related setting parameter </param>
        /// <returns></returns>
        private object RuntimeRead(PropertyInfo prop, string key, string startIdentifier = "")
        {
            var instanceType = prop.PropertyType;
            var hash = key.GetHashCode();

            object instance = CacheInstance(instanceType, hash, startIdentifier);

            return instance;
        }

        /// <summary>
        /// Caches an instance so that it won't need to be reistantiated
        /// </summary>
        /// <param name="instanceType">Runtime type of the instance</param>
        /// <param name="hash">Hash of the instance type</param>
        /// <returns></returns>
        private object CacheInstance(Type instanceType, int hash, string startIdentifier = "")
        {
            object instance;

            if (!_cache.TryGetValue(hash, out instance))
            {
                instance = Activator.CreateInstance(instanceType);
                _cache.Add(hash, instance);
                Update(instance, startIdentifier);
            }

            return instance;
        }


        /// <summary>
        /// Reads or Updates the properties' value of an instance
        /// </summary>
        /// <param name="instance">The instance that requires to update it properties</param>
        /// <param name="startIdentifier">A prefix for the name of the related setting parameter</param>
        private void Update(object instance, string startIdentifier)
        {
            var props = ParseAllProperties(instance.GetType());
            AssignProperties(instance, props, startIdentifier);
        }


        /// <summary>
        /// Applies modification to one instance (deprecated)
        /// </summary>
        /// <typeparam name="T">The instance type</typeparam>
        /// <param name="instance">The instance as an object</param>
        [Obsolete]
        public void Save<T>(object instance) where T : class => RuntimeSave(instance, null);


        /// <summary>
        /// Applies the modifications made to one or more instances
        /// </summary>
        /// <param name="instances">params array of instances to be saved</param>
        public void Save(params object[] instances)
        {
            foreach (var instance in instances) RuntimeSave(instance, "");
        }


        /// <summary>
        /// Applies the modifications made to an instance and its eventual sub-classes
        /// </summary>
        /// <param name="instance">The base instyances that sourcers the updates</param>
        /// <param name="startIdentifier">A prefix for the name of the related setting parameter </param>
        private void RuntimeSave(object instance, string startIdentifier = "")
        {
            if (instance is null) throw new NullReferenceException("The datatype requires to be a valid instance hooked to the same manager");

            var props = ParseAllProperties(instance.GetType());
            foreach ((var attr, var prop) in props)
            {
                if (attr.GetType() == typeof(GroupIdentifierAttribute))
                {
                    RuntimeSave(prop.GetValue(instance), $"{attr.ResourceIdentifier}_");
                }
                else
                {
                    _preferences.Set($"{startIdentifier}{attr.ResourceIdentifier}", prop.GetValue(instance), attr.ValueType);
                }
            }
        }

        /// <summary>
        /// Reads values stored in preferences and assignes them to the respective property of an instance
        /// </summary>
        /// <param name="instance">The instance</param>
        /// <param name="props">Enumerable collection of properties of the instance</param>
        /// <param name="startIdentifier">A prefix for the name of the related setting parameter </param>
        private void AssignProperties(object instance, IEnumerable<(IdentifierAttribute, PropertyInfo)> props, string startIdentifier)
        {
            foreach ((var attr, var prop) in props)
            {
                if (attr.GetType() == typeof(GroupIdentifierAttribute))
                {
                    var value = RuntimeRead(prop, attr.ResourceIdentifier, $"{startIdentifier}{attr.ResourceIdentifier}_");
                    prop.SetValue(instance, value);
                }
                else
                {
                    object value = _preferences.Get($"{startIdentifier}{attr.ResourceIdentifier}", attr.DefaultValue, attr.ValueType);
                    prop.SetValue(instance, value);
                }

            }
        }

        /// <summary>
        /// Parses properties of a given type
        /// </summary>
        /// <param name="type">The type that needs to be parsed</param>
        /// <returns>An IEnumerable of pairs providing PropertyInfo with its respective property</returns>
        public static IEnumerable<(IdentifierAttribute, PropertyInfo)> ParseAllProperties(Type type)
        {
            IList<PropertyInfo> res = new List<PropertyInfo>();
            foreach (var prop in type.GetProperties())
            {
                var attr = prop.GetCustomAttributes(typeof(IdentifierAttribute), false).Cast<IdentifierAttribute>().FirstOrDefault();

                if (attr != null) yield return (attr, prop);
            }
        }

        private static IEnumerable<(GroupIdentifierAttribute, PropertyInfo)> ParseGroupProperties(Type type)
        {
            IList<PropertyInfo> res = new List<PropertyInfo>();
            foreach (var prop in type.GetProperties())
            {
                var attr = prop.GetCustomAttributes(typeof(GroupIdentifierAttribute), false).Cast<GroupIdentifierAttribute>().FirstOrDefault();

                if (attr != null) yield return (attr, prop);
            }
        }

        /// <summary>
        /// Removes the binding of a given class from its preferences
        /// </summary>
        /// <typeparam name="T">The type of the class</typeparam>
        public void Unhook<T>() where T : class
        {
            var stack = new Stack<Type>();

            var startType = typeof(T);
            _cache.Remove(startType.GetHashCode());
            stack.Push(startType);

            while (stack.Count != 0)
            {
                var type = stack.Pop();
                var props = ParseGroupProperties(type);

                foreach ((var attr, var prop) in props)
                {
                    _cache.Remove(attr.ResourceIdentifier.GetHashCode());
                    stack.Push(prop.GetType());
                }
            }
        }
    }
}