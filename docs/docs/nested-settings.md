---
sidebar_position: 2
---

# Nested Settings

In some cases, your settings class might include a **sub-class** that contains some properties mapped as Identifiers. For example:

```cs title="model/Settings.cs"

public class Settings 
{
    [Impostor.Attributes.Identifier("Name", true)]
    public bool Name { get; set; }

    [Impostor.Attributes.DateIdentifier("SubscriptionDate", "now")]
    public DateTime SubscriptionDate { get; set; }

    [Impostor.Attributes.Identifier("PreferredNumber", 20)]
    public int PreferredNumber { get; set; }

    [Impostor.Attributes.Identifier("Color", (int)System.Drawing.KnownColor.Blue)]
    public System.Drawing.KnownColor Color { get; set; }

    // highlight-next-line
    public ConnectionSettings Connection { get; set; }  // how to handle that?

    public string SayHi() => $"Hi, I'm {Name}";
}

```

```cs title="model/ConnectionSettings.cs"
public class ConnectionSettings
{
    [Impostor.Attributes.Identifier("IP", "127.0.0.1")]
    public string IpAddress { get; set; }
}
```

In order to access **ConnectionSettings** from the parent class (which, in this case, is Setting), you can use the `Impostor.Attributes.GroupIdentifierAttribute`. The latter requires to only explicit a name, that will be used to compose the path to the specific sub-setting inside the preferences storage.

Here's an example on **how the Settings class should look like**.

```cs title="model/Settings.cs"
public class Settings 
{
    [Impostor.Attributes.Identifier("Name", true)]
    public bool Name { get; set; }

    [Impostor.Attributes.DateIdentifier("SubscriptionDate", "now")]
    public DateTime SubscriptionDate { get; set; }

    [Impostor.Attributes.Identifier("PreferredNumber", 20)]
    public int PreferredNumber { get; set; }

    [Impostor.Attributes.Identifier("Color", (int)System.Drawing.KnownColor.Blue)]
    public System.Drawing.KnownColor Color { get; set; }

    // highlight-next-line
    [Impostor.Attributes.GroupIdentifier("Connection")]
    // highlight-next-line
    public ConnectionSettings Connection { get; set; } 

    public string SayHi() => $"Hi, I'm {Name}";
}
```

After doing that, you will be able to **hook the impostor manager** to the parent class only. The instance returned will allow you to access the `Connection` object, whose properties will be equal to the currently stored value of the respectively mapped setting (or the default one otherwise).
