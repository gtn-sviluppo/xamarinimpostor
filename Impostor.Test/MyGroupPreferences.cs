﻿using Impostor.Attributes;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Impostor.Test
{
    internal class MyGroupPreferences : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler? PropertyChanged;

        private MyPreferences _group;


        [GroupIdentifier("Group1")]
        public MyPreferences Group
        {
            get => _group;
            set => SetProperty(out _group, value);
        }

        public void SetProperty<T>(out T output, T value, [CallerMemberName] string memberName = "")
        {
            output = value;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(memberName));
        }
    }
}