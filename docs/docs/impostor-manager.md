--- 
sidebar_position: 3
---

# Impostor Manager

**Impostor Manager** is the core concept that allows you to store and retrive your settings anywhere in your code. Once instantiated, the latter can be used to **hook** your previously defined classes to your storage and populate their properties with the respectively stored value or, otherwise, the default one.

## Instantiate your Manager 

You can instantiate a manager with the following line of code

```cs
var manager = new ImpostorManager(new PreferencesImplementation());
```

where **PreferencesImplementation** corresponds to the one contained inside `Xamarin.Essentials.Implementation`. You can of course use other implementations, as long as they use the `IPreferences` interface (defined inside `Xamarin.Essentials.Interfaces`).

## Hook to a class

You can now **hook** the manager to the `Settings` class defined in the previous section, with the following line of code:

```cs
var settings = manager.Hook<Settings>();
```

### Note 

The `Hook<T>()` method will provide a reference to an instance that will constantly reflect any changes applied to the current and other instances of the same class. This mean that you will be able to hook two times to the same class keeping the changes synchronized. 

In other words,

```cs
var settings = manager.Hook<Settings>();
var settings2 = manager.Hook<Settings>();

settings.Name = "newname";

// highlight-next-line
Console.WriteLine(settings.Name == settings2.Name);   // true
```

## Unhook an instance

You may want to unhook (or unbind) an instance from your manager, in order to do that you can run:

```cs
manager.Unhook(settings2);

settings.Name = "newname";

//highlight-next-line
Console.WriteLine(settings.Name == settings2.Name);   // false
```

Since the `settings2` instance was previously **unhooked**, the modifications applied to the other instance are not reflected on the other one.

:::warning

Unhooking is something that is not commonly required unless in some specific cases and may be a cause of hard-to-debug runtime errors in your application.

:::

## Saving you changes

In order to allows your settings to **persist between restarts** you are required to **Save** your instances once you are sure that you want to apply all the modifications. 

In order to do that, you can run the `Save()` method with one or more instances of your classes as parameters.

```cs
manager.Save(settings);
```

:::tip 

You can also run `manager.Save(settings, instanceofanotherclass, ...);`

:::

## Complete use case

Here's a complete example that shows how settings can be **accessed**, **modified** and **saved**.

```cs
var manager = new ImpostorManager(new PreferencesImplementation());

var settings = manager.Hook<Settings>();

settings.Name = "newname";
settings.SubscriptionDate = "2021-12-01";
settings.Color = System.Drawing.KnownColor.Red;

manager.Save(settings);
```

