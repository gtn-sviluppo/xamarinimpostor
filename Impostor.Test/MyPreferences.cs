﻿using Impostor.Attributes;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Impostor.Test
{
    internal class MyPreferences : INotifyPropertyChanged
    {

        private string _url;
        private string _username;
        private string _password;
        private int _age;
        private DateTime _day;
        private bool _isEnabled;


        [Identifier("Url", "url-default")]
        public string Url
        {
            get => _url;
            set => SetProperty(out _url, value);
        }

        [Identifier("Username", "username-default")]
        public string Username
        {
            get => _username;
            set => SetProperty(out _username, value);
        }

        [Identifier("Password", "pass-default")]
        public string Password
        {
            get => _password;
            set => SetProperty<string>(out _password, value);
        }

        [Identifier("Age", 20)]
        public int Age
        {
            get => _age;
            set => SetProperty(out _age, value);
        }

        [DateIdentifier("Date", "now")]
        public DateTime Day
        {
            get => _day;
            set => SetProperty(out _day, value);
        }

        [Identifier("IsEnabled", true)]
        public bool IsEnabled
        {
            get => _isEnabled;
            set => SetProperty(out _isEnabled, value);
        }

        public string DataMatrix => $"{Username}:{Password}";

        public event PropertyChangedEventHandler? PropertyChanged;

        public void SetProperty<T>(out T output, T value, [CallerMemberName] string memberName = "")
        {
            output = value;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(memberName));
        }
    }
}