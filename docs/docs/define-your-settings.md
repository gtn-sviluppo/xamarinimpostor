---
sidebar_position: 1
---

# Define your settings 

With XamarinImpostor you can define your settings by just creating a **class** in your model and assigning specific **attributes** to its **properties**.

## An Example

```cs title="/model/Settings.cs"
public class Settings 
{
    [Impostor.Attributes.Identifier("Name", true)]
    public bool Name { get; set; }

    [Impostor.Attributes.DateIdentifier("SubscriptionDate", "now")]
    public DateTime SubscriptionDate { get; set; }

    [Impostor.Attributes.Identifier("PreferredNumber", 20)]
    public int PreferredNumber { get; set; }

    [Impostor.Attributes.Identifier("Color", (int)System.Drawing.KnownColor.Blue)]
    public System.Drawing.KnownColor Color { get; set; }

    public string SayHi() => $"Hi, I'm {Name}";
}
```

### Overview

The `Impostor.Attributes.Identifier` attribute allows you to define both the **name** that will be used by the storage in order to save an retrieve your setting and the **default value** that will be used if the setting is not found.

Impostor will **infer** the type of your setting according to its default value. Available types are:

- **int**
- **long**
- **double**
- **float**
- **string**
- **bool**

**Enum** values can be casted to **int** in order to be saved (as shown in the example). 

You can also save **DateTime** values by using the `Impostor.Attributes.DateIdentifier` attribute, which allows you to define a date with the following string values:

- **now** will declare `DateTime.Now` as a default value
- **min** will declare `DateTime.Min` as a default value
- **max** will declare `DateTime.Max` as a default value
- Any other value will be parsed as a `yyyy-MM-dd` formatted string

## Creating custom extensions

If you want, you can create a custom attributes by extending the `Impostor.Attributes.IdentifierAttribute` in your code. Because, under the hood, Xamarin.Essentials.Preferences allows the storage of the previously declared types of values, you will need to incorporate an encoding and decoding logic in order to store your value as a string or binary data and retrieve it casting it to your specific type.

Here's an example regarding how the **DateIdentifierAttribute** is defined

```cs
public class DateIdentifierAttribute : IdentifierAttribute
{
    public DateIdentifierAttribute(string name, string defaultValue) : 
                base(name, CalculateDateParam(defaultValue) {}

    private static DateTime CalculateDateParam(string defaultValue)
    {
        switch (defaultValue)
        {
            case "now": return DateTime.Now;
            case "min": return DateTime.MinValue;
            case "max": return DateTime.MaxValue;
            default: return DateTime.ParseExact(defaultValue, "yyyy-MM-dd", null);
        }
    }
}
```

## Other properties and methods

Any property, method or variable that does not provide an **IdentifierAttribute** will not be considered by the **ImpostorManager**. You will not need to assign values to properties or to instantiate them manually, because everything will be handled **automatically** in order to keep them binded to your actual settings values.

The next section will describe how to use the ImpostorManager in order to manage the life-cycle of your settings classes.
