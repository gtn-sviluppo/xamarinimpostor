---
sidebar_position: 0
---

# Intro

**XamarinImpostor** can be included very easily in your projects and can be, then, used to manage your application's settings as they were properties just like any other class. 

## Getting Started

Once you have scaffholded your **cross-platform** Xamarin project, you can add **XamarinImpostor** as an external dependency using nuget. All you need to do is run: 

```bash
dotnet add package Impostor --version 0.4.8
```

The package can be also found on [nuget.org](https://www.nuget.org/packages/Impostor/)

### Dependencies 

- .NET Standard 2.0 or higher
- Xamarin.Forms
- Xamarin.Essentials.Implementation
- Xamarin.Essentials.Interfaces
    - The latter will be installed by XamarinImpostor itself as its own dependency

## Usage

**XamarinImpostor** rotates around the concepts of `Hook()`, `Unhook()` and `Save()`. Once a settings class has been hooked to the so called **Impostor Manager**, this can be retrieved anywhere in your code and its properties can be accessed and modified. Once saved, the current value of all properties will be written on storage using the underlying implementation of Xamarin.Essentials.Preferences. You can also unhook a class in order to remove any binding from the manager.

An **in-depth** guide on how to use **Impostor Manager** is presented in the next section.. You can also unhook a class in order to remove any binding from the manager.

An **in-depth** guide on how to use **Impostor Manager** is presented in the next section.
