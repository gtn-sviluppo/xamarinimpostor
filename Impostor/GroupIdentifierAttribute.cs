﻿using System;

namespace Impostor.Attributes
{
    /// <summary>
    /// Identifies a group of properties with a prefix
    /// that all the subproperties will inherit
    /// </summary>
    public class GroupIdentifierAttribute : IdentifierAttribute
    {
        /// <summary>
        /// Constructs a group setting param
        /// </summary>
        /// <param name="name">The identifier of the group</param>
        public GroupIdentifierAttribute(string name) : base(name, null) { }
    }
}