﻿using Impostor.Attributes;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Impostor.Test
{
    public class ImpostazioniFTP : BaseNotifier
    {
        private FTPParams _ftpInConfiguration;
        private FTPParams _ftpOutConfiguration;
        private bool _shareInOut;

        [GroupIdentifier("FTP_IN")]
        public FTPParams FtpInConfiguration
        {
            get => _ftpInConfiguration;
            set => SetProperty(out _ftpInConfiguration, value);
        }

        [GroupIdentifier("FTP_OUT")]
        public FTPParams FtpOutConfiguration
        {
            get => _ftpOutConfiguration;
            set => SetProperty(out _ftpOutConfiguration, value);
        }

        [Identifier("FTP_ShareInOut", true)]
        public bool ShareInOut
        {
            get => _shareInOut;
            set => SetProperty(out _shareInOut, value);
        }
    }

    public class FTPParams : BaseNotifier
    {

        private string _url;             // URL server FTP
        private int _porta;              // porta di ascolto
        private string _root;            // root directory
        private string _utente;          // utente
        private string _password;        // password

        protected override void SetProperty<T>(out T output, T value, [CallerMemberName] string propertyName = "")
        {
            base.SetProperty(out output, value, $"{propertyName}{this.GetHashCode()}");
        }

        [Identifier("FTPUrl", "")]
        public string URL
        {
            get => _url;
            set => SetProperty(out _url, value);
        }

        [Identifier("FTPPort", 21)]
        public int Porta
        {
            get => _porta;
            set => SetProperty(out this._porta, value);
        }

        [Identifier("FTPRoot", "")]
        public string Root
        {
            get => _root;
            set => SetProperty(out _root, value);
        }

        [Identifier("FTPUser", "")]
        public string Utente
        {
            get => _utente;
            set => SetProperty(out _utente, value);
        }


        [Identifier("FTPPassword", "")]
        public string Password
        {
            get => _password;
            set => SetProperty(out _password, value);
        }
    }
}
