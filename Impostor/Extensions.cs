﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Essentials.Interfaces;

namespace Impostor.Extensions
{
    public static class Extensions
    {
        /// <summary>
        /// Extends the Get method of IPreferences in order to get values with runtime types
        /// </summary>
        /// <param name="preferences">the IPreferences object that stores settings</param>
        /// <param name="resourceIdentifier">The string that identifies the specific setting</param>
        /// <param name="defaultValue">The default value as an object</param>
        /// <param name="valueType">The runtime type of the default value</param>
        /// <returns></returns>
        public static object Get(this IPreferences preferences, string resourceIdentifier, object defaultValue, Type valueType)
        {
            var methods = typeof(IPreferences).GetMethods().Where(x => x.Name.StartsWith("Get"));
            foreach (var method in methods)
            {
                if (method.GetParameters().Count() != 2) continue;

                var param = method.GetParameters().SingleOrDefault(p => p.Name == "defaultValue" && p.ParameterType == valueType);

                if (param == null) continue;

                return method.Invoke(preferences, new object[] { resourceIdentifier, defaultValue });


            }
            return null;
        }

        /// <summary>
        /// Extends the Set method of IPreferences in order to set values with runtime types
        /// </summary>
        /// <param name="preferences">the IPreferences object that stores settings</param>
        /// <param name="resourceIdentifier">The string that identifies the specific setting</param>
        /// <param name="defaultValue">The value that will be assigned, as an object</param>
        /// <param name="valueType">The runtime type of the value</param>
        public static void Set(this IPreferences preferences, string resourceIdentifier, object value, Type valueType)
        {
            foreach (var method in typeof(IPreferences).GetMethods().Where(x => x.Name.StartsWith("Set")))
            {
                if (method.GetParameters().Count() != 2) continue;

                var param = method.GetParameters().SingleOrDefault(p => p.Name == "value" && p.ParameterType == valueType);

                if (param == null) continue;

                method.Invoke(preferences, new object[] { resourceIdentifier, value });
                return;
            }
        }
    }
}
