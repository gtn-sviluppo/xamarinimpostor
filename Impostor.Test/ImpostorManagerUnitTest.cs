using System;
using Xamarin.Essentials.Interfaces;
using Xunit;
using Impostor.Extensions;
using Impostor;

namespace Impostor.Test
{
    public class ImpostorManagerUnitTest
    {

        [Fact]
        public void TestPopulateWithDefaults()
        {
            IPreferences _pref = new MockPreferences();
            ImpostorManager i = new ImpostorManager(_pref);

            MyPreferences p = i.Hook<MyPreferences>();

            Assert.Equal("url-default", p.Url);
            Assert.Equal("username-default", p.Username);
            Assert.Equal("pass-default", p.Password);
            Assert.Equal("username-default:pass-default", p.DataMatrix);

            _pref.Clear();
        }

        [Fact]
        public void TestClassIdMatch()
        {
            IPreferences _pref = new MockPreferences();
            ImpostorManager i = new ImpostorManager(_pref);

            MyPreferences p = i.Hook<MyPreferences>();
            MyPreferences p2 = i.Hook<MyPreferences>();

            p2.Username = "Danny";
            p2.Age = 43;

            i.Save(p);
            
            Assert.Equal(p.GetType().GetHashCode(), p2.GetType().GetHashCode());
        }

        [Fact]
        public void TestCachedInstance()
        {
            IPreferences _pref = new MockPreferences();
            ImpostorManager i = new ImpostorManager(_pref);

            MyPreferences p = i.Hook<MyPreferences>();
            MyPreferences p2 = i.Hook<MyPreferences>();

            p.Username = "Giacomo";
            i.Save(p);

            Assert.Equal(p.Username, p2.Username);
        }

        [Fact]
        public void TestGroupPropertyRead()
        {
            IPreferences _pref = new MockPreferences();
            ImpostorManager i = new ImpostorManager(_pref);

            MyGroupPreferences p = i.Hook<MyGroupPreferences>();

            Assert.Equal("username-default", p.Group.Username);

            p.Group.Username = "test";
            Assert.Equal("test", p.Group.Username);

            _pref.Clear();
        }

        [Fact]
        public void TestGroupPropertySave()
        {
            IPreferences _pref = new MockPreferences();
            ImpostorManager i = new ImpostorManager(_pref);

            MyGroupPreferences p = i.Hook<MyGroupPreferences>();

            p.Group.Username = "test";
            i.Save(p);

            Assert.Equal("test", p.Group.Username);
            Assert.Equal("test", _pref.Get("Group1_Username", ""));

            _pref.Clear();
        }

        [Fact]
        public void TestDeleteInstance()
        {
            IPreferences _pref = new MockPreferences();
            ImpostorManager i = new ImpostorManager(_pref);

            MyPreferences p = i.Hook<MyPreferences>();

            p.Username = "test";
            i.Save(p);

            MyPreferences p2 = i.Hook<MyPreferences>();

            Assert.Equal(p.Username, p2.Username);

            i.Unhook<MyPreferences>();

            p.Username = "test2";
            Assert.Equal("test2", p2.Username);

            MyPreferences p3 = i.Hook<MyPreferences>();

            Assert.Equal("test", p3.Username);

            _pref.Clear();
        }

        [Fact]
        public void TestSaveWrongInstance()
        {
            IPreferences _pref = new MockPreferences();
            ImpostorManager i = new ImpostorManager(_pref);

            var ftp = new ImpostazioniFTP();

            Assert.Throws<NullReferenceException>(() => i.Save(ftp));
        }

        [Fact]
        public void TestGroupedSave()
        {
            IPreferences _pref = new MockPreferences();
            ImpostorManager i = new ImpostorManager(_pref);

            var p = i.Hook<ImpostazioniFTP>();

            Assert.Equal("", p.FtpInConfiguration.Root);
            Assert.Equal("", p.FtpOutConfiguration.Root);

            p.FtpInConfiguration.Root = "/in";
            p.FtpOutConfiguration.Root = "/out";

            Assert.Equal("/in", p.FtpInConfiguration.Root);
            Assert.Equal("/out", p.FtpOutConfiguration.Root);

            i.Save(p);

            i.Unhook<ImpostazioniFTP>();

            var p2 = i.Hook<ImpostazioniFTP>();

            Assert.Equal("/in", p2.FtpInConfiguration.Root);
            Assert.Equal("/out", p2.FtpOutConfiguration.Root);
        }

        [Fact]
        public void TestRecursiveDelete()
        {
            IPreferences _pref = new MockPreferences();
            ImpostorManager i = new ImpostorManager(_pref);

            MyGroupPreferences p = i.Hook<MyGroupPreferences>();

            Assert.Equal("username-default", p.Group.Username);

            p.Group.Username = "first";

            i.Save(p);

            Assert.Equal("first", p.Group.Username);
            Assert.Equal("first", _pref.Get("Group1_Username", ""));

            MyGroupPreferences p2 = i.Hook<MyGroupPreferences>();

            Assert.Equal("first", p2.Group.Username);

            i.Unhook<MyGroupPreferences>();

            MyGroupPreferences p3 = i.Hook<MyGroupPreferences>();

            Assert.Equal("first", p3.Group.Username);

            i.Unhook<MyGroupPreferences>();

            _pref.Clear();
        }

        [Fact]
        public void TestMultipleSave()
        {
            IPreferences _pref = new MockPreferences();
            ImpostorManager i = new ImpostorManager(_pref);

            var ftp = i.Hook<ImpostazioniFTP>();
            var gp = i.Hook<MyGroupPreferences>();

            ftp.FtpInConfiguration.URL = "newinurl";
            ftp.FtpOutConfiguration.URL = "newouturl";

            gp.Group.Username = "newusername";

            i.Save(ftp, gp);

            i.Unhook<ImpostazioniFTP>();
            i.Unhook<MyGroupPreferences>();

            var newFtp = i.Hook<ImpostazioniFTP>();
            var newGp = i.Hook<MyGroupPreferences>();

            Assert.Equal("newinurl", newFtp.FtpInConfiguration.URL);
            Assert.Equal("newouturl", newFtp.FtpOutConfiguration.URL);
            Assert.Equal("newusername", newGp.Group.Username);
        }

        [Fact]
        public void TestSave()
        {
            IPreferences _pref = new MockPreferences();
            var d = DateTime.ParseExact("2022-05-11", "yyyy-MM-dd", null);

            _pref.Set("Date", d);

            ImpostorManager i = new ImpostorManager(_pref);
            var p = i.Hook<MyPreferences>();

            Assert.Equal("url-default", p.Url);
            Assert.Equal("username-default", p.Username);
            Assert.Equal("pass-default", p.Password);
            Assert.Equal(20, p.Age);

            p.Url = "https://www.gtngroup.it/";
            p.Username = "andrea";
            p.Age = 22;
            p.Day = d;

            i.Save(p);

            Assert.Equal("https://www.gtngroup.it/", p.Url);
            Assert.Equal("andrea", p.Username);
            Assert.Equal("pass-default", p.Password);
            Assert.Equal(22, p.Age);
            Assert.Equal(d, p.Day);

            var newP = i.Hook<MyPreferences>();

            Assert.Equal("https://www.gtngroup.it/", newP.Url);
            Assert.Equal("andrea", newP.Username);
            Assert.Equal("pass-default", newP.Password);
            Assert.Equal(22, newP.Age);
            Assert.Equal(d, newP.Day);

            _pref.Clear();

        }

        [Fact]
        public void TestRead()
        {
            IPreferences _pref = new MockPreferences();
            _pref.Set("Url", "https://www.google.it");
            _pref.Set("Username", "User");
            _pref.Set("Password", "Pass");

            ImpostorManager i = new ImpostorManager(_pref);
            MyPreferences p = i.Hook<MyPreferences>();

            Assert.Equal("https://www.google.it", p.Url);
            Assert.Equal("User", p.Username);
            Assert.Equal("Pass", p.Password);
            Assert.Equal(20, p.Age);

            _pref.Clear();
        }

        [Fact]
        public void TestPreferencesGetExtension()
        {
            IPreferences _pref = new MockPreferences();
            _pref.Set("Url", "https://www.google.it");
            _pref.Set("Date", DateTime.MinValue);

            Assert.Equal("https://www.google.it", _pref.Get("Url", ""));
            Assert.Equal("https://www.google.it", _pref.Get("Url", "", typeof(String)));
            Assert.Equal(DateTime.MinValue, _pref.Get("Date", DateTime.Today, typeof(DateTime)));

            _pref.Clear();

        }

        [Fact]
        public void TestPreferencesSetExtension()
        {
            IPreferences _pref = new MockPreferences();
            _pref.Set("Url", "https://www.google.it", typeof(string));
            _pref.Set("Date", DateTime.MinValue, typeof(DateTime));

            Assert.Equal("https://www.google.it", _pref.Get("Url", "", typeof(string)));
            Assert.Equal(DateTime.MinValue, _pref.Get("Date", DateTime.Now, typeof(DateTime)));

            _pref.Clear();

        }

        [Fact]
        public void TestAttributeParser()
        {
            var attrs = ImpostorManager.ParseAllProperties(typeof(MyPreferences));

            Assert.Collection(attrs,
                el => Assert.Equal("Url", el.Item1.ResourceIdentifier),
                el2 => Assert.Equal("Username", el2.Item1.ResourceIdentifier),
                el3 => Assert.Equal("Password", el3.Item1.ResourceIdentifier),
                el4 => Assert.Equal("Age", el4.Item1.ResourceIdentifier),
                el5 => Assert.Equal("Date", el5.Item1.ResourceIdentifier),
                el6 => Assert.Equal("IsEnabled", el6.Item1.ResourceIdentifier));

            Assert.Collection(attrs,
                el => Assert.Equal("Url", el.Item2.Name),
                el2 => Assert.Equal("Username", el2.Item2.Name),
                el3 => Assert.Equal("Password", el3.Item2.Name),
                el4 => Assert.Equal("Age", el4.Item2.Name),
                el5 => Assert.Equal("Day", el5.Item2.Name),
                el6 => Assert.Equal("IsEnabled", el6.Item2.Name));
        }
    }
}