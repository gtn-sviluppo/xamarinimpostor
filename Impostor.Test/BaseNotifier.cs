﻿using System.ComponentModel;

namespace Impostor.Test
{
    public abstract class BaseNotifier : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        protected virtual void SetProperty<T>(out T output, T value, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            output = value;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(memberName));
        }
    }
}