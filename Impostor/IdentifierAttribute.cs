﻿using System;

namespace Impostor.Attributes
{
    /// <summary>
    /// Identifies the name of the setting mapped to a property,
    /// together with its default value
    /// </summary>
    public class IdentifierAttribute : Attribute
    {
        public string ResourceIdentifier { get; set; }
        public object DefaultValue { get; set; }
        public Type ValueType { get; set; }

        /// <summary>
        /// Defines a string preference
        /// </summary>
        /// <param name="name">The resource identifier</param>
        /// <param name="defaultValue">The value return when no preference is defined</param>
        public IdentifierAttribute(string name, string defaultValue)
        {
            ResourceIdentifier = name;
            DefaultValue = defaultValue;
            ValueType = typeof(string);
        }

        /// <summary>
        /// Defines a boolean preference
        /// </summary>
        /// <param name="name">The resource identifier</param>
        /// <param name="defaultValue">The value return when no preference is defined</param>
        public IdentifierAttribute(string name, bool defaultValue)
        {
            ResourceIdentifier = name;
            DefaultValue = defaultValue;
            ValueType = typeof(bool);
        }

        /// <summary>
        /// Defines an int preference
        /// </summary>
        /// <param name="name">The resource identifier</param>
        /// <param name="defaultValue">The value return when no preference is defined</param>
        public IdentifierAttribute(string name, int defaultValue)
        {
            ResourceIdentifier = name;
            DefaultValue = defaultValue;
            ValueType = typeof(int);
        }

        /// <summary>
        /// Defines a long preference
        /// </summary>
        /// <param name="name">The resource identifier</param>
        /// <param name="defaultValue">The value return when no preference is defined</param>
        public IdentifierAttribute(string name, long defaultValue)
        {
            ResourceIdentifier = name;
            DefaultValue = defaultValue;
            ValueType = typeof(long);
        }

        /// <summary>
        /// Defines a DateTime preference (DateIdentifierAttribute should be employed for public scope)
        /// </summary>
        /// <param name="name">The resource identifier</param>
        /// <param name="defaultValue">The value return when no preference is defined</param>
        internal IdentifierAttribute(string name, DateTime defaultValue)
        {
            ResourceIdentifier = name;
            DefaultValue = defaultValue;
            ValueType = typeof(DateTime);
        }
    }
}